clean:
	rm -rf ~/.vim/bundle
	rm -rf ~/.vim/plugged

config:
	ln -sf ~/.vim/vimrc ~/.vimrc

install: clean config
	vim +PlugInstall +qall
