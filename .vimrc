"=============[ Plugin Related]=============
" Helps force plugins to load correctly when it is turned back on below
filetype off

" Load Vundle Plugins
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" Bracket Completions
Plugin 'cohama/lexima.vim'
" slurpy
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-repeat'
Plugin 'fatih/vim-go'
call vundle#end()


" For plugins to load correctly
filetype plugin indent on

" Lexima Config
let g:lexima_enable_basic_rules=1


"=============[ Custom Configs ]=============

" Pick a leader key
let mapleader=" "

" Don't try to be vi compatible
set nocompatible
" Turn on syntax highlighting
syntax on

" Security
set modelines=0

" Show line numbers
set number

" Show file stats
set ruler

" Encoding
set encoding=utf-8

" Whitespace
set wrap
set formatoptions=tcqrn1
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set noshiftround

" Cursor motion
set scrolloff=3
set backspace=indent,eol,start
set matchpairs+=<:> " use % to jump between pairs
runtime! macros/matchit.vim

" Move up/down editor lines - basically handles wrapped lines better
nnoremap j gj
nnoremap k gk

" Allow hidden buffers
set hidden

" Rendering
set ttyfast

" Status bar
set laststatus=2
set statusline+=%F

" Last line
set showmode
set showcmd

" Searching
nnoremap / /\v
vnoremap / /\v
set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch
" Clear search
map <leader><space> :let @/ = ""<CR>

" Remap help key.
inoremap <F1> <ESC>:set invfullscreen<CR>a
nnoremap <F1> :set invfullscreen<CR>
vnoremap <F1> :set invfullscreen<CR>

" Formatting
map <leader>q gqip
" Remove Whitespace on save
autocmd FileType c,cpp,java,php,python,perl,r autocmd BufWritePre <buffer> %s/\s\+$//e
" Visualize tabs and newlines
set listchars=tab:▸\ ,eol:¬
" Uncomment this to enable by default:
" set list " To enable by default
" Or use your leader key + l to toggle on/off
map <leader>l :set list!<CR> " Toggle tabs and EOL

" Color scheme
set background=dark
" put https://github.com/Lokaltog/vim-monotone.git
" in ~/.vim/colors/  
set termguicolors
colorscheme monotone-terminal

"============[ Golang Config ]============
set autowrite  " Auto saves on :GoBuild or :make
" Move to next or previous error, close error win
map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR> 

" Build, test and run shortcuts
autocmd FileType go nmap <leader>r  <Plug>(go-run)
autocmd FileType go nmap <leader>t  <Plug>(go-test)
" run :GoBuild or :GoTestCompile based on the go file
function! s:build_go_files()
  let l:file = expand('%')
  if l:file =~# '^\f\+_test\.go$'
    call go#test#Test(0, 1)
  elseif l:file =~# '^\f\+\.go$'
    call go#cmd#Build(0)
  endif
endfunction

autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>
" Use goimports instead of gofmt
let g:go_fmt_command = "goimports"
